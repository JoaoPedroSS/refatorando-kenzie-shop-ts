import FormCadastro from "../components/FormCadastro";
import FormLogin from "../components/Login";
import { Switch, Route } from "react-router-dom";
import Cart from "../pages/Cart";
import ListProducts from "../pages/Products";
import { useCartContext } from "../providers/cart";

const Routes = () => {
  const { cart, favorites } = useCartContext();
  return (
    <Switch>
      <Route exact path="/">
        <ListProducts products={cart} isFavorites={false} />
      </Route>
      <Route path="/cart">
        <Cart favorite={favorites} isFavorites={true} />
      </Route>
      <Route path="/cadastro">
        <FormCadastro />
      </Route>
      <Route path="/login">
        <FormLogin />
      </Route>

    </Switch>
  );
};

export default Routes;
