import "./App.css";
import Routes from "./routes/routes";
import { useCartContext } from "./providers/cart";
function App() {
  const { cart, favorites } = useCartContext();
  console.log(favorites);
  return (
    <div className="App">
      <header className="App-header">
        <Routes />
      </header>
    </div>
  );
}

export default App;
