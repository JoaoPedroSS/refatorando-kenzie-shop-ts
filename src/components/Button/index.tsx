import { ReactNode } from "react";
import { ButtonStyled } from "./styles";



interface IButtonProps {
    children: ReactNode;
    deleted?: boolean;
    onClick: () => void;

}
const Button = ({deleted, children, onClick} : IButtonProps) => {
    return(
        <ButtonStyled isDeleted={deleted} onClick={onClick}>
            {children}
        </ButtonStyled>
    )
}

export default Button;