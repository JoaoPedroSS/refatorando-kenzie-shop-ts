import styled, { css } from "styled-components";

interface IButtonStyled {
  isDeleted?: boolean;
}


export const ButtonStyled = styled.button<IButtonStyled>`
  margin-top: 5px;
  width: 150px;
  font-size: 18px;
  height: 32px;
  border: none;
  border-radius: 10px;
  background-color: deepskyblue;
  outline: none;
  cursor: pointer;

  ${(props) =>
    props.isDeleted &&
    css`
      width: 150px;
    `};
`;