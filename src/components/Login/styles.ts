import styled from "styled-components";

export const Form = styled.form`
  background-color: burlywood;
  width: 400px;
  height: 400px;
  border-radius: 10px;
  padding: 10px;
`;

export const Label = styled.label`
  color: black;
`;

export const Input = styled.button`
  height: 35px;
  width: 200px;
  border: 0;
  margin: 5px;
  border-radius: 5px;
`;

export const Paragrafo = styled.p`
  color: red;
  font-size: 20px;
`;

export const Button = styled.button`
  width: 100px;
  height: 35px;
  background-color: black;
  color: white;
  border: 0;
  border-radius: 5px;
`;

export const Box = styled.div`
  font-size: 20px;
  margin-top: 20px;
`;
