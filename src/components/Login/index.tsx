import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import axios from "axios";
import { Form, Label, Input, Paragrafo, Button, Box } from "./styles";
import { Link } from "react-router-dom";


interface IFormInputs {
  email: string;
  password: string;
}

const FormLogin = () => {
  const schema = yup.object().shape({
    email: yup.string().required("Campo Obrigatório").email(),
    password: yup.string().required("Campo Obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: IFormInputs) => {
    console.log(data);
    axios
      .post(" https://kenzieshop2.herokuapp.com/login", data)
      .then((response) => console.log(response))
      .catch((err) => console.log("deu erro"));
  };

  return (
    <div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Label>Digite seu Email</Label> <br></br>
        <Input placeholder="email" {...register("email")} />
        <Paragrafo>{errors.email?.message}</Paragrafo>
        <Label>Digite seu Senha</Label> <br></br>
        <Input placeholder="Senha" {...register("password")} />
        <Paragrafo>{errors.password?.message}</Paragrafo>
        <Button type="submit">Enviar</Button>
        <Box>
          <p>
            Não tem uma conta ainda{" "}
            <Link to="/cadastro" style={{ fontSize: "15px" }}>
              Clique aqui para criar
            </Link>
          </p>
        </Box>
      </Form>
    </div>
  );
};

export default FormLogin;
