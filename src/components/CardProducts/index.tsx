import { useCartContext } from "../../providers/cart";
import { Container, Image, Paragrafo, Box } from "./styles";
import Button from "../Button/index";

interface Products {
  name: string;
  price: number;
  id: number;
  description: string;
  image_url: string;
}

interface HeaderProps {
  cart: Products;
  isFavorites?: boolean;
}

const CardProducts = ({ cart, isFavorites = false }: HeaderProps) => {
  const { addCart, removeCart } = useCartContext();

  const { description, image_url, name, price } = cart;

  return (
    <div>
      <Container>
        <Box>
          <Image src={image_url} />
          <Paragrafo>Nome: {name}</Paragrafo>
          <Paragrafo>Preço: R${price}</Paragrafo>
          <Paragrafo>Descrição: {description}</Paragrafo>
          {isFavorites ? (
            <Button deleted={true} onClick={() => removeCart(cart)}>
              Remove
            </Button>
          ) : (
            <Button onClick={() => addCart(cart)}>Adicionar</Button>
          )}
        </Box>
      </Container>
    </div>
  );
};

export default CardProducts;
