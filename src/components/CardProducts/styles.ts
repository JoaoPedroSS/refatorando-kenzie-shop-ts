import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const Box = styled.div`
  border: 2px solid black;
  border-radius: 5px;
  width: 400px;
  margin: 10px;
  padding: 10px;
`;

export const Image = styled.img`
  width: 200px;
  height: 370px;
  margin: 15px auto;
`;

export const Paragrafo = styled.p`
  color: black;
`;
