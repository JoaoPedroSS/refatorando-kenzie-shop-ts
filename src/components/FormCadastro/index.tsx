import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import axios from "axios";
import { Link, useHistory } from "react-router-dom";
import { Form, Label, Input, Paragrafo, Button, Box} from "./styles";

interface IFormInputs {
  username: string;
  email: string;
  password: string;
}

const FormCadastro = () => {
  const history = useHistory();

  const schema = yup.object().shape({
    username: yup.string().required(),
    email: yup.string().required().email(),
    password: yup.string().required(),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: IFormInputs) => {
    console.log(data);
    axios
      .post(" https://kenzieshop2.herokuapp.com/register", data)
      .then((response) => {
        console.log(response);
        history.push("/login");
      })
      .catch((err) => console.log("deu erro"));
  };
  

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Label>Digite seu Nome</Label> <br></br>
      <Input placeholder="Nome" {...register("username")} />
      <Paragrafo>{errors.username?.message}</Paragrafo>
      <Label>Digite seu Email</Label> <br></br>
      <Input placeholder="email" {...register("email")} />
      <Paragrafo>{errors.email?.message}</Paragrafo>
      <Label>Digite seu Senha</Label> <br></br>
      <Input placeholder="Senha" {...register("password")} />
      <Paragrafo>{errors.password?.message}</Paragrafo>
      <Button type="submit">Enviar</Button>
      <Box>
        <p>Já tem uma conta<Link to="/login">Clique aqui</Link></p>
      </Box>
    </Form>
  );
};

export default FormCadastro;
