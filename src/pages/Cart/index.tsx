import { useHistory } from "react-router-dom";
import CardProducts from "../../components/CardProducts";
import { useCartContext } from "../../providers/cart";
import Button from "../../components/Button/index";

interface Products {
  name: string;
  price: number;
  id: number;
  description: string;
  image_url: string;
}
interface ProductsProps {
  favorite: Products[];
  isFavorites?: boolean;
}

const Cart = ({ favorite, isFavorites = false }: ProductsProps) => {
  const history = useHistory();
  const { favorites } = useCartContext();

  return (
    <div>
      <p style={{ color: "black" }}>Quantidade de Items: {favorites.length}</p>
      <p style={{ color: "black" }}>
        Valor Total: R$
        {favorites.reduce((acc, current) => acc + current.price, 0)} Reais
      </p>
      <div
        style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
      >
        {favorite.map((item, index) => (
          <CardProducts key={index} cart={item} isFavorites={isFavorites} />
        ))}
      </div>

      <Button onClick={() => history.push("/")}>Voltar</Button>
    </div>
  );
};

export default Cart;
