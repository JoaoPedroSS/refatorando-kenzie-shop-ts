import { useCartContext } from "../../providers/cart";
import CardProducts from "../../components/CardProducts";
import { useHistory, Link } from "react-router-dom";
import Button from "../../components/Button/index";

interface Products {
  name: string;
  price: number;
  id: number;
  description: string;
  image_url: string;
}
interface ProductsProps {
  products: Products[];
  isFavorites?: boolean;
}

const ListProducts = ({ products, isFavorites = false }: ProductsProps) => {
  const { favorites } = useCartContext();
  const history = useHistory();
  return (
    <div>
      <Button onClick={() => history.push("/cart")}>
        Carrinho: {favorites.length}
      </Button>
      <Button onClick={() => history.push("/login")}>Login</Button>
      <div
        style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
      >
        {products.map((product, index) => (
          <CardProducts key={index} cart={product} isFavorites={isFavorites} />
        ))}
      </div>
    </div>
  );
};

export default ListProducts;
