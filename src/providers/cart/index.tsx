import { useEffect } from "react";
import { useContext } from "react";
import { useState } from "react";
import { createContext, ReactNode } from "react";

interface Products {
  name: string;
  price: number;
  id: number;
  description: string;
  image_url: string;
}

interface ProductsCart {
  children: ReactNode;
}

interface CartProducts {
  cart: Products[];
  favorites: Products[];
  addCart: (cart: Products) => void;
  removeCart: (cart: Products) => void;
}

const CartContext = createContext<CartProducts>({} as CartProducts);

export const CartProvider = ({ children }: ProductsCart) => {
  const [cart, setCart] = useState<Products[]>([] as Products[]);
  const [favorites, setFavorites] = useState<Products[]>([]);

  useEffect(() => {
    fetch("https://kenzieshop2.herokuapp.com/products")
      .then((response) => response.json())
      .then((response) => setCart([...response]))
      .catch((err) => console.log("deu ruim!"));
  }, []);

  const addCart = (cart: Products) => {
    setFavorites([...favorites, cart]);
  };

  const removeCart = (cartDeleted: Products) => {
    const newList = favorites.filter(
      (remove) => remove.name !== cartDeleted.name
    );
    setFavorites(newList);
  };


  return (
    <CartContext.Provider value={{ cart, addCart, removeCart,favorites }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCartContext = () => useContext(CartContext);
